# Google-Map-Customized-Search

## Description

Google Map Customised Search fetches with auto suggestions based on name which exists in database and marker placed in Map based on Latitute and Logitude.

## Introduction - the project's aim

The projects main aim is to connect cutomized locations with auto suggestions from database and marker will be placed based on it position(Latitude and Logitude).

## Technologies

Python 3.6    
Flask framework
Angular 8    
GoogleMaps API
Elasticsearch

## Screenshots
frontend/src/assets/Screenshots

## Video Link
